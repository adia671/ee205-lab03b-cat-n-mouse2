#!/bin/bash


DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

echo -e  "OK cat, I’m thinking of a number from 1 to $THE_MAX_VALUE. Make a guess:"


while ((aGuess !=  THE_NUMBER_IM_THINKING_OF));
do
	read -p "Enter guess: " aGuess
	if ((aGuess <  1));
	then
		echo  "You must enter a number that’s >= 1"
	elif ((aGuess >  THE_MAX_VALUE));
	then
		echo "You must enter a number that’s <= $THE_MAX_VALUE "
	elif ((aGuess >  THE_NUMBER_IM_THINKING_OF));
	then
		echo "No cat... the number I’m thinking of is smaller than $aGuess"
	elif ((aGuess < THE_NUMBER_IM_THINKING_OF));
	then
		echo "No cat... the number I’m thinking of is larger than $aGuess"
	fi
done

echo -e "You got me. \n"
echo " /\\_/\\   "
echo "( o.o )   "
echo " > ^ <   "
	


